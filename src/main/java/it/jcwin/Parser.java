package it.jcwin;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.StringField;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Parser {
	final static Logger logger = LoggerFactory.getLogger(Parser.class);
	
	private IndexWriter index;
	
	public Parser(Directory directory) throws IOException {
		//Create index
		this.index = getIndexWriter(directory);
	}
	
	private HashMap<String, String> getIngredientsAndAmount(Document doc) {
		HashMap<String, String> ingredientsAndAmount = new HashMap<String, String>();
		Elements liIngredients = doc.getElementsByClass("ingredient");
		
		for(Element liIngredient : liIngredients) {
			Element aIngredient = liIngredient.getElementsByClass("ingredient").get(0);
			Element ingredient = aIngredient.getElementsByTag("a").get(0);
			Element amount = liIngredient.getElementsByClass("amount").get(0);
			ingredientsAndAmount.put(ingredient.html().trim().toLowerCase(), amount.html().trim().toLowerCase());
		}
		
		return ingredientsAndAmount;
	}
	
	private String getPrepTime(Document doc) {
		Elements prepTime = doc.getElementsByClass("preptime");

		if(prepTime.size() == 0) {
			return null;
		}
		
		return prepTime.get(0).ownText().trim().toLowerCase();
	}
	
	private String getCookTime(Document doc) {
		Elements cooktime = doc.getElementsByClass("cooktime");
		
		if(cooktime.size() == 0) {
			return null;
		}
		
		return cooktime.get(0).ownText().trim().toLowerCase();
	}
	
	private String getDifficultyOfPreparation(Document doc) {
		Elements difficultyOfPreparation = doc.getElementsByClass("difficolta");

		if(difficultyOfPreparation.size() == 0) {
			return null;
		}
		
		return difficultyOfPreparation.get(0).ownText().trim().toLowerCase();
	}
	
	private String getDoses(Document doc) {
		Elements spanDoses = doc.getElementsByClass("dosixpers");
		
		if(spanDoses.size() == 0) {
			return null;
		}
		
		Elements doses = spanDoses.get(0).getElementsByClass("yield");
		
		if(doses.size() == 0) {
			return null;
		}
		
		return doses.get(0).ownText().trim().toLowerCase();
	}
	
	private String getCost(Document doc) {
		Elements costs = doc.getElementsByClass("costo");
		
		if(costs.size() == 0) {
			return null;
		}
		
		Element cost = costs.get(0);
		return cost.ownText().trim().toLowerCase();
	}
	
	private List<String> getCategories(Document doc) {
		List<String> categories = new ArrayList<String>();
		Elements tags = doc.getElementsByClass("tags");
		
		if(tags.size() == 0) {
			return null;
		}
		
		Elements tag = tags.get(0).getElementsByClass("tag");
		
		for(Element category : tag) {
			categories.add(category.html().trim().toLowerCase());
		}
		
		return categories;
	}
	
	private String getName(Document doc) {
		Elements names = doc.getElementsByClass("item");
		
		if(names.size() == 0) {
			return null;
		}
		
		Element name = names.get(0);
		return name.html().replace("<!-- <EdIndex> -->", "").replace("\n<!-- </EdIndex> -->", "").trim().toLowerCase();
	}
	
	public void parseDocument(Document doc) throws IOException {
		String name = getName(doc);
		
		//prendo gli Ingredienti e le quantita
		HashMap<String, String> ingredientsAndAmount = getIngredientsAndAmount(doc); 
		
		//prendo il tempo di preparazione
		String prepTime = getPrepTime(doc);
		
		//prendo il tempo di cottura
		String cookTime = getCookTime(doc);
		
		//prendo la difficoltà di preparazione
		String difficultyOfPreparation = getDifficultyOfPreparation(doc);
		
		//prendo le dosi
		String doses = getDoses(doc);
		
		//prendo il costo
		String cost = getCost(doc);
		
		//prendo le categorie
		List<String> categories = getCategories(doc);
		
		logger.debug("name: "+name);
		logger.debug("ingredientsAndAmount: "+ingredientsAndAmount);
		logger.debug("prepTime: "+prepTime);
		logger.debug("cooktime: "+cookTime);
		logger.debug("difficultyOfPreparation: "+difficultyOfPreparation);
		logger.debug("doses: "+doses);
		logger.debug("cost: "+cost);
		logger.debug("categories: "+categories+"\n");
		
		//Add document to index
		org.apache.lucene.document.Document document = new org.apache.lucene.document.Document();
		
		for(String key : ingredientsAndAmount.keySet()) {
			Field fingredientsAndAmount = new StringField(key, ingredientsAndAmount.get(key), Field.Store.YES);
		    document.add(fingredientsAndAmount);
		}
	    
	    Field fName = new StringField("name", name, Field.Store.YES);
	    document.add(fName);
		
		if(prepTime != null) {
			Field fprepTime = new StringField("prepTime", prepTime, Field.Store.YES);
		    document.add(fprepTime);
		}
	    
	    if(cookTime != null) {
		    Field fcookTime = new StringField("cookTime", cookTime, Field.Store.YES);
		    document.add(fcookTime);
	    }
	    
	    if(difficultyOfPreparation != null) {
		    Field fdifficultyOfPreparation = new StringField("difficolta", difficultyOfPreparation, Field.Store.YES);
		    document.add(fdifficultyOfPreparation);
	    }
		    
	    if(doses != null) {
	    	Field fdoses = new StringField("dosi", doses, Field.Store.YES);
	    	document.add(fdoses);
	    }
	    
	    if(cost != null) {
	    	Field fcost = new StringField("costo", cost, Field.Store.YES);
	    	document.add(fcost);
	    }
	    
	    if(categories != null) {
	    	for(String category : categories) {
			    Field fcategories = new StringField("categoria", category, Field.Store.YES);
			    document.add(fcategories);
	    	}
	    }
	    
	    index.addDocument(document);
	    index.commit();
	}
	
	private IndexWriter getIndexWriter(Directory directory) throws IOException {
	    Analyzer analyzer = new StandardAnalyzer(Version.LUCENE_40);
	    IndexWriterConfig config = new IndexWriterConfig(Version.LUCENE_40, analyzer);
	    config.setOpenMode(IndexWriterConfig.OpenMode.CREATE);
	    return new IndexWriter(directory, config);
	}
	
	public void close() throws IOException {
		//Close index
		index.close();
	}	
}