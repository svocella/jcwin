package it.jcwin.crawling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class DFS implements CrawlingAlgorithm {
	public String home;
	final static Logger logger = LoggerFactory.getLogger(DFS.class);
	private int numeroMaxRicette;
	private int j = 0, z = 0;

	public DFS(String home, int numeroMaxRicette){
		this.home=home;
		this.numeroMaxRicette=numeroMaxRicette;
	}

	public HashMap<String, String> find(ArrayList<String> filters) {
		HashMap<String,ArrayList<String>> pagine = new HashMap<String,ArrayList<String>>();
		HashMap<String,String> pagineVisitate = new HashMap<String,String>();
		HashMap<String,String> ricette = new HashMap<String,String>();

		//String home="http://www.giallozafferano.it/ricette/Acqua-di-rose";

		Document htmlhome = ScannerHTML.getHTML(home);
		logger.debug(home);
		//logger.debug(htmlhome);
		pagine.put(home, ScannerHTML.getAllLinks(htmlhome,this.home,filters));
		pagineVisitate.put(home,"g");
		j=0;
		for(String s :pagine.get(home)){
			if(!pagineVisitate.containsKey(s)) pagineVisitate.put(s,"w");
		}

		logger.debug(Integer.toString(pagine.get(home).size()));	

		while(!ScannerHTML.isAllBlack(pagineVisitate)&&ricette.size()<numeroMaxRicette){
			Iterator<String> l = pagine.get(home).iterator();
			while(l.hasNext()&&ricette.size()<numeroMaxRicette){
				String s= l.next();
				if(pagineVisitate.get(s).equals("w")) {
					logger.debug((++j)+" - ricette: "+ricette.size()+" - "+home+" - FIND - "+s+" - WHITE");
					find(filters,s,ricette,pagineVisitate,pagine);
				}
				if(pagineVisitate.get(s).equals("g")) logger.debug(j+" - ricette: "+ricette.size()+" - "+home+" - is GRAY: "+s);
				if(pagineVisitate.get(s).equals("b")) logger.debug(j+" - ricette: "+ricette.size()+" - "+home+" - is BLACK: "+s);
			}
			pagineVisitate.put(home,"b");
		}
		int w=0,b=0,g=0;
		for(String s:pagineVisitate.keySet()){
			if(pagineVisitate.get(s).equals("w")){
				w++;
			}
			if(pagineVisitate.get(s).equals("b")){
				b++;
			}
			if(pagineVisitate.get(s).equals("g")){
				g++;
			}
		}
		logger.debug("WHITE:"+w+"/"+(w+b+g)+" - GRAY:"+g+"/"+(w+b+g)+" - BLACK:"+b+"/"+(w+b+g));
		return ricette;
	}



	private void find(ArrayList<String> filters, String s1,HashMap<String, String> ricette,HashMap<String, String> pagineVisitate, HashMap<String, ArrayList<String>> pagine) {
		logger.debug((z++)+" - ricette: "+ricette.size()+" - in FIND - "+s1);
		Document htmls1 = ScannerHTML.getHTML(s1);
		if(ScannerHTML.isRobotsEnabled(htmls1)){
			logger.debug(s1);
			//logger.debug(htmls1);
			addRecipe(ricette,s1);
			pagine.put(s1, ScannerHTML.getAllLinks(htmls1,this.home,filters));
			pagineVisitate.put(s1,"g");

			for(String s :pagine.get(s1)){
				if(!pagineVisitate.containsKey(s)) pagineVisitate.put(s,"w");
			}
			logger.debug(Integer.toString(pagine.get(s1).size()));

			Iterator<String> l = pagine.get(s1).iterator();
			while(l.hasNext()&&ricette.size()<numeroMaxRicette){
				String s= l.next();
				if(pagineVisitate.get(s).equals("w")) {
					logger.debug((++j)+" - ricette: "+ricette.size()+" - "+s1+" - FIND - "+s+" - WHITE");
					find(filters,s,ricette,pagineVisitate,pagine);
				}
				if(pagineVisitate.get(s).equals("g")) logger.debug(j+" - ricette: "+ricette.size()+" - "+s1+" - is GRAY: "+s);
				if(pagineVisitate.get(s).equals("b")) logger.debug(j+" - ricette: "+ricette.size()+" - "+s1+" - is BLACK: "+s);
			}
			pagineVisitate.put(s1,"b");

		}
		else{
			pagineVisitate.put(s1,"b");
		}
	}

	private  void addRecipe(HashMap<String,String> ricette, String link){
		Document r = ScannerHTML.getHTML(link);
		logger.debug("ricette: "+(ricette.size()+1)+" - "+ScannerHTML.isRicetta(r)+" - "+link);
		if(ScannerHTML.isRicetta(r) && !ricette.keySet().contains(link)) ricette.put(link,r.html());
	}

}
