package it.jcwin.crawling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;

import org.jsoup.nodes.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class BFS implements CrawlingAlgorithm {
	final static Logger logger = LoggerFactory.getLogger(BFS.class);
	private String home;
	private int numeroMaxRicette;
	
	public BFS(String home,int numeroMaxRicette){
		this.home=home;
		this.numeroMaxRicette=numeroMaxRicette;
	}
	
	public HashMap<String,String> find(ArrayList<String> filters) {
		HashMap<String,ArrayList<String>> pagine = new HashMap<String,ArrayList<String>>();
		HashMap<String,String> pagineVisitate = new HashMap<String,String>();
		HashMap<String,String> ricette = new HashMap<String,String>();

		Document htmlhome = ScannerHTML.getHTML(home);
		logger.debug(home);
		pagine.put(home, ScannerHTML.getAllLinks(htmlhome,home,filters));
		pagineVisitate.put(home,"g");
		Iterator<String> p = pagine.get(home).iterator();
		while(p.hasNext()&&ricette.size()<numeroMaxRicette){
			String s = p.next();
			if(!pagineVisitate.containsKey(s)) pagineVisitate.put(s,"g");
			if(pagineVisitate.get(s).equals("g")) addReceipt(s,ricette,pagineVisitate);
		}
		pagineVisitate.put(home,"b");
		logger.debug(Integer.toString(pagine.get(home).size()));

		int k=0;
		
		while(!ScannerHTML.isAllBlack(pagineVisitate)&&ricette.size()<numeroMaxRicette){
			HashMap<String,String> copia = (HashMap<String, String>) pagineVisitate.clone();
			Iterator<String> l = pagineVisitate.keySet().iterator();
			//logger.debug("l.size: "+pagineVisitate.keySet().size());
			while(l.hasNext()&&ricette.size()<numeroMaxRicette){
				String s= l.next();
				k++;
				Document htmls=ScannerHTML.getHTML(s);
				//logger.debug(s);
				if(ScannerHTML.isRobotsEnabled(htmls)){
					if(pagineVisitate.get(s).equals("g")){
						pagine.put(s, ScannerHTML.getAllLinks(htmls,home,filters));
					}
					logger.debug(k+"/"+copia.keySet().size()+" - Numero ricette trovate: "+ricette.size());
					Iterator<String> h = pagine.get(s).iterator();
					while(h.hasNext()&&ricette.size()<numeroMaxRicette){
						String s1 = h.next();
						if(!copia.containsKey(s1)) copia.put(s1,"g");
						if(copia.get(s1).equals("g")) addReceipt(s1,ricette,copia);
					}
				}
				copia.put(s,"b");
			}
			pagineVisitate = copia;

		}
		
		int b=0,g=0;
		for(String s:pagineVisitate.keySet()){
			if(pagineVisitate.get(s).equals("b")){
				b++;
			}
			if(pagineVisitate.get(s).equals("g")){
				g++;
			}
		}

		logger.debug(" - GRAY:"+g+"/"+(b+g)+" - BLACK:"+b+"/"+(b+g));
		return ricette;
	}
	
	private void addReceipt(String s,HashMap<String,String> ricette,HashMap<String,String> copia){
		Document r = ScannerHTML.getHTML(s);
		if(ScannerHTML.isRobotsEnabled(r)){
			boolean isricetta = ScannerHTML.isRicetta(r);
			logger.debug("Ricette: "+(ricette.size())+" - "+isricetta+" - "+s);
			if(isricetta && !ricette.keySet().contains(s)) ricette.put(s,r.html());
		}
		else{
			copia.put(s, "b");
		}
	}
}
