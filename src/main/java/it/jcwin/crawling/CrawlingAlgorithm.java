package it.jcwin.crawling;

import java.util.ArrayList;
import java.util.HashMap;

//CrawlingAlgorithm that use Strategy Pattern
public interface CrawlingAlgorithm {
	public HashMap<String,String> find (ArrayList<String> filters);
}
