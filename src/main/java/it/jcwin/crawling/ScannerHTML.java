package it.jcwin.crawling;

import java.util.ArrayList;
import java.util.HashMap;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ScannerHTML {
	final static Logger logger = LoggerFactory.getLogger(ScannerHTML.class);

	public static Document getHTML(String urlToRead) {
		try {
			if(!urlToRead.contains("javascript:")&&!urlToRead.contains(".css")&&!urlToRead.contains("blog.")&&!urlToRead.contains(".ico")){
				return Jsoup.connect(urlToRead).get();
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			logger.debug("ERRORE di CONNESSIONE - "+urlToRead);
		}
		return new Document("<html /html>");
	}

	public static boolean isRobotsEnabled(Document html){
		Elements e = html.getElementsByAttribute("name");
		
		for(Element l:e){
			String robot = l.attr("name");
			String robotRule = l.attr("content");
			//logger.debug(robot+" - "+robotRule);
			if(robot.equals("Robots") && !robotRule.equals("NOINDEX")){
				//logger.debug(robot+" - "+robotRule);
				return true;
			}
		}
		return false;
	}

	public static ArrayList<String> getAllLinks(Document html,String home,ArrayList<String> filters){
		ArrayList<String> links = new ArrayList<String>();
		Elements e = html.getElementsByAttribute("href");
		for(Element l:e){
			String link = l.attr("href");

			if(!isWellFormedLink(link)){
				link=home+link;
			}
			if(!links.contains(link)&&ScannerHTML.isWellFormedLink(link)&&ScannerHTML.isAcceptedLink(filters, link)){
				logger.debug(link);
				links.add(link);
			}

		}
		return links;
	}

	public static boolean isAcceptedLink(ArrayList<String> filters,String link){
		for(String s:filters){
			if(!link.contains(s)){
				return false;
			}
		}
		return true;
	}

	public static boolean isWellFormedLink(String s){
		if((!s.contains("http://")&&!s.contains("Http://")&&!s.contains("https://")&&!s.contains("Https://"))){
			return false;
		}
		else{
			int nHTTP =3;
			if(s.contains("http://")){
				return s.split("http://").length<nHTTP;
			}
			if(s.contains("https://")){
				return s.split("https://").length<nHTTP;
			}
			if(s.contains("Http://")){
				return s.split("Http://").length<nHTTP;
			}
			if(s.contains("Https://")){
				return s.split("Https://").length<nHTTP;
			}
			else {
				return false;
			}
		}
	}

	public static boolean isRicetta(Document html){
		Elements e = html.getElementsByAttribute("name");
		for(Element l:e){
			String ricetta = l.attr("name");
			String ricettanome = l.attr("content");
			if(ricetta.equals("EdTitle")&&!ricettanome.equals("")){
				logger.debug(ricetta+" - "+ricettanome);
				return true;
			}
		}
		return false;
	}

	public static boolean isAllBlack(HashMap<String,String> h){
		for(String s:h.keySet()){
			if(!s.equals("b"))return false;
		}
		return true;
	}
}
