package it.jcwin.shared;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.Directory;

/**
 * The client side stub for the RPC service.
 */
public interface InformationRetrivalService {
	public HashMap<String,String> gathering(String home, Integer maxNumber, String typeOfAlgorithm, ArrayList<String> filters) throws NoSuchAlgorithmException;
	public void parsing(Directory directory, HashMap<String,String> recipes) throws IOException, NullPointerException;
	public void search(Directory directory, String query) throws ParseException, IOException;
}
