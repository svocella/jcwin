package it.jcwin;

import it.jcwin.server.InformationRetrivalServiceImpl;
import it.jcwin.shared.InformationRetrivalService;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class App 
{
	final static Logger logger = LoggerFactory.getLogger(App.class);
	
	public static void main(String[] args) {
		String home = "http://www.giallozafferano.it";
		
		try
		{
			//Create directory
			Directory directory = FSDirectory.open(new File("index"));
			
			//Create the IR system
			InformationRetrivalService ir = new InformationRetrivalServiceImpl();
			
			//Filters for the gathering part
			ArrayList<String> filters=new ArrayList<String>();
			filters.add("giallozafferano.it");
			
			System.out.println("Enter a command (type help to see the documentation and quit to exit): ");
			InputStreamReader converter = new InputStreamReader(System.in);
			BufferedReader in = new BufferedReader(converter);
			String input = "";
			String[] commands;
			while (!(input.equals("quit"))){
				System.out.print("> ");
				input = in.readLine().trim();
				commands = input.split(" ");
				
				if (commands[0].equals("index")){
					Integer maxNumber = Integer.parseInt(commands[1]);
					String typeOfAlgorithm = commands[2];
					
					//gathering part
					HashMap<String, String> recipes = ir.gathering(home, maxNumber, typeOfAlgorithm, filters);
					
					//parsing part
					ir.parsing(directory, recipes);
				}
				if (commands[0].equals("search")){
					String query = input.substring("search ".length()); //FIXME Fix this
					
					//search engine part
					ir.search(directory, query);
				}
				if (commands[0].equals("help")){
					System.out.println("type ");
					System.out.println("- index N A - where N is the MAX number of receipts to download from www.giallozafferano.com and A is the type of algorithm (DFS or BFS, e.g. index 1 BFS)");
					System.out.println("- search Q - where Q is the query to the index of www.giallozafferano.com");
					System.out.println("- help - to see this");
					System.out.println("- quit - to exit");
				}
			}
		
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}