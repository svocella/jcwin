package it.jcwin.server;

import it.jcwin.Parser;
import it.jcwin.SearchEngine;
import it.jcwin.crawling.BFS;
import it.jcwin.crawling.Crawler;
import it.jcwin.crawling.DFS;
import it.jcwin.shared.InformationRetrivalService;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.store.Directory;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

/**
 * The server side implementation of the RPC service.
 */
@SuppressWarnings("serial")
public class InformationRetrivalServiceImpl implements InformationRetrivalService {
	public HashMap<String,String> gathering(String home, Integer maxNumber, String typeOfAlgorithm, ArrayList<String> filters) throws NoSuchAlgorithmException {
		Crawler src;
		if(typeOfAlgorithm.equalsIgnoreCase("BFS")){
			src= new Crawler(new BFS(home, maxNumber));
		}
		else {
			if(typeOfAlgorithm.equalsIgnoreCase("DFS")){
				src=  new Crawler(new DFS(home, maxNumber));
			} else {
				throw new NoSuchAlgorithmException();
			}
		}
		
		return src.findReceipt(filters);
	}
	
	public void parsing(Directory directory, HashMap<String,String> recipes) throws IOException, NullPointerException {
		Parser parser = new Parser(directory);
		
		if(recipes == null) {
			throw new NullPointerException();
		}
		
		for(String ricetta : recipes.keySet()){
			Document doc = Jsoup.parse(recipes.get(ricetta));
			parser.parseDocument(doc);
		}
		parser.close();
	}
	
	public void search(Directory directory, String query) throws ParseException, IOException {
		SearchEngine searchEngine = new SearchEngine(directory);
		searchEngine.getResults(query, 20);
	}
}
