package it.jcwin;

import java.io.IOException;

import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.util.Version;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SearchEngine {
	final Logger logger = LoggerFactory.getLogger(SearchEngine.class);
	
	private Directory directory;
	
	public SearchEngine(Directory directory) {
		this.directory = directory;
	}
	
	public void getResults(String query, int nresults) throws ParseException, IOException {
		logger.debug("query: {}", query);
		logger.debug("nresults: {}", nresults);
		
		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		QueryParser parser = new QueryParser(Version.LUCENE_40, "categoria", new StandardAnalyzer(Version.LUCENE_40));
		Query queryL = parser.parse(query);
		
		logger.debug("queryL: {}", queryL);
		
		TopDocs results = searcher.search(queryL, nresults);
		
		logger.debug("results.totalHits: {}", results.totalHits);
		
		ScoreDoc[] hits = results.scoreDocs;
		
		logger.debug("hits.length: {}", hits.length);
		
		for (int i = 0; i < hits.length; i++) {
			Document doc = searcher.doc(hits[i].doc);
			String name = doc.get("name");
			logger.debug("result: "+name);
		}
	}
}
